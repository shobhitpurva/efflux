import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyA3JTmQRQnKadvXf_BkUb-NtyYDmiaVzdM",
  authDomain: "efflux-b7c31.firebaseapp.com",
  projectId: "efflux-b7c31",
  storageBucket: "efflux-b7c31.appspot.com",
  messagingSenderId: "53227032234",
  appId: "1:53227032234:web:88e459deb798acac69cad3",
  measurementId: "G-TDJD0QHX87"
  // apiKey: "AIzaSyA9BnlX96fMf7XiUVCFRsoQzG8DGERJkeY",
  // authDomain: "disneyplus-clone-a33d5.firebaseapp.com",
  // projectId: "disneyplus-clone-a33d5",
  // storageBucket: "disneyplus-clone-a33d5.appspot.com",
  // messagingSenderId: "37918794208",
  // appId: "1:37918794208:web:dbe9842dfe1dda522a4b85",
  // measurementId: "G-DRVLJKWRWG",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
const storage = firebase.storage();

export { auth, provider, storage };
export default db;
